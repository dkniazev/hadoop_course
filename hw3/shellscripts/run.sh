#!/usr/bin/env bash
# first version
hadoop jar ~/Downloads/hadoop_course/hw3/target/byteperrequest-1.0-SNAPSHOT.jar /test3/000000.txt /out3
# with compression
hadoop jar ~/Downloads/hadoop_course/hw3/target/byteperrequest-1.0-SNAPSHOT.jar /test3/000000.txt /out3 true
# read compressed file
hadoop fs -text /out3/part-r-00000.snappy