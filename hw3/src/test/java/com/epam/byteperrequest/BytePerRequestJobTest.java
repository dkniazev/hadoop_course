package com.epam.byteperrequest;

import com.epam.byteperrequest.datatype.ByteSizeWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author Dmitrii_Kniazev
 * @since 11/18/2016
 */
public class BytePerRequestJobTest {
    private static final LongWritable ONE_LONG = new LongWritable(1);

    private static final List<String> EXAMPLES = Arrays.asList(
            "ip1566 - - [27/Apr/2011:22:28:50 -0400] \"HEAD /sunFAQ/serial/ HTTP/1.0\" 200 512 \"-\" \"nph-churl\"",
            "ip1 - - [24/Apr/2011:04:06:01 -0400] \"GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028 \"-\" \"Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)\"",
            "ip46 - - [27/Apr/2011:22:24:05 -0400] \"GET /sunFAQ/faq_framebuffer/framebuffer.html HTTP/1.1\" 304 - \"-\" \"Sogou web spider/4.0(+http://www.sogou.com/docs/help/webmasters.htm#07)\"",
            "ip1565 - - [27/Apr/2011:22:27:27 -0400] \"GET /sun_ipx/ HTTP/1.1\" 200 11586 \"-\" \"MLBot (www.metadatalabs.com/mlbot)\"",
            "ip1566 - - [27/Apr/2011:22:28:50 -0400] \"HEAD /sunFAQ/serial/ HTTP/1.0\" 200 0 \"-\" \"nph-churl\"",
            "ip19 - - [24/Apr/2011:05:14:36 -0400] \"GET /vanagon/VanagonProTraining/DigiFant/016.jpg HTTP/1.1\" 200 109813 \"http://www.thesamba.com/vw/forum/viewtopic.php?t=387697&highlight=flat\" \"Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10_5_8; en-us) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4\""
    );
    private MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;
    private ReduceDriver<Text, IntWritable, Text, ByteSizeWritable> reduceDriver;
    private MapReduceDriver<LongWritable, Text, Text, IntWritable, Text, ByteSizeWritable> mapReduceDriver;

    @Before
    public void setUp() {
        final BytePerRequestMapper mapper = new BytePerRequestMapper();
        mapDriver = MapDriver.newMapDriver(mapper);

        final BytePerRequestReducer reducer = new BytePerRequestReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);

        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapper() throws IOException {
        for (String str : EXAMPLES) {
            mapDriver.withInput(ONE_LONG, new Text(str));
        }
        mapDriver.withAllOutput(Arrays.asList(
                new Pair<>(new Text("ip1566"), new IntWritable(512)),
                new Pair<>(new Text("ip1"), new IntWritable(40028)),
                new Pair<>(new Text("ip46"), new IntWritable(0)),
                new Pair<>(new Text("ip1565"), new IntWritable(11586)),
                new Pair<>(new Text("ip1566"), new IntWritable(0)),
                new Pair<>(new Text("ip19"), new IntWritable(109813))

        ));
        mapDriver.runTest();

    }

    @Test
    public void testReducer() throws IOException {
        reduceDriver.withInput(new Text("ip1"), Arrays.asList(
                new IntWritable(1),
                new IntWritable(2),
                new IntWritable(3)
        ));
        reduceDriver.withInput(new Text("ip88"), Arrays.asList(
                new IntWritable(55),
                new IntWritable(0),
                new IntWritable(22)
        ));
        reduceDriver.withOutput(new Text("ip1"), new ByteSizeWritable("2.0,6"));
        reduceDriver.withOutput(new Text("ip88"), new ByteSizeWritable("25.7,77"));
        reduceDriver.runTest();
    }

    @Test
    public void testMapReduce() throws IOException {
        for (String str : EXAMPLES) {
            mapReduceDriver.withInput(ONE_LONG, new Text(str));
        }
        mapReduceDriver.withOutput(new Text("ip1"), new ByteSizeWritable("40028.0,40028"));
        mapReduceDriver.withOutput(new Text("ip1565"), new ByteSizeWritable("11586.0,11586"));
        mapReduceDriver.withOutput(new Text("ip1566"), new ByteSizeWritable("256.0,512"));
        mapReduceDriver.withOutput(new Text("ip19"), new ByteSizeWritable("109813.0,109813"));
        mapReduceDriver.withOutput(new Text("ip46"), new ByteSizeWritable("0.0,0"));

        mapReduceDriver.runTest();
    }
}
