package com.epam.byteperrequest;

/**
 * @author Dmitrii_Kniazev
 * @since 23.11.16
 */
public enum WebClientsCounters {
    MOZILLA,
    IE,
    OTHER
}
