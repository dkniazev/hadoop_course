package com.epam.byteperrequest;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * @author Dmitrii_Kniazev
 * @since 11/18/2016
 */
public class BytePerRequestJob extends Configured implements Tool {
    public static void main(String[] args) throws Exception {
        final int exitCode = ToolRunner.run(new BytePerRequestJob(), args);
        System.exit(exitCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int run(String[] args) throws Exception {
        final Configuration conf = getConf();
        conf.set("mapreduce.output.textoutputformat.separator", ",");

        final GenericOptionsParser optionParser = new GenericOptionsParser(conf, args);
        final String[] remainingArgs = optionParser.getRemainingArgs();
        if (remainingArgs.length < 2) {
            System.err.println("Current number of parameters" + remainingArgs.length);
            System.err.println("Usage: byteperrequest <in> <out> [compressed]");
            return 2;
        }

        if (args.length >= 3 && "true".equals(args[2])) {
            //Whether to compress the final job outputs (true or false)
            conf.set("mapreduce.output.fileoutputformat.compress", "true");
            //If the final job outputs are to be compressed, which codec should be used.
            conf.set("mapreduce.output.fileoutputformat.compress.codec",
                    "org.apache.hadoop.io.compress.SnappyCodec");
            //For SequenceFile outputs, what type of compression should be used (NONE, RECORD, or BLOCK).
            //BLOCK is recommended.
            conf.set("mapreduce.output.fileoutputformat.compress.type", "BLOCK");
        }
        final Job job = Job.getInstance(conf, "BytePerRequestHomework");
        job.setJarByClass(getClass());
        //input file
        TextInputFormat.addInputPath(job, new Path(args[0]));
        job.setInputFormatClass(TextInputFormat.class);
        //output file
        TextOutputFormat.setOutputPath(job, new Path(args[1]));
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setMapperClass(BytePerRequestMapper.class);
        job.setReducerClass(BytePerRequestReducer.class);

        final boolean isCompleted = job.waitForCompletion(true);
        //print counters
        final Counters counters = job.getCounters();
        for (final WebClientsCounters webClient : WebClientsCounters.values()) {
            final Counter counter = counters.findCounter(webClient);
            System.out.println(counter.getDisplayName() + ": " + counter.getValue());
        }

        if (isCompleted) {
            return 0;
        }
        return 1;
    }
}
