package com.epam.byteperrequest;

import com.epam.byteperrequest.datatype.ByteSizeWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author Dmitrii_Kniazev
 * @since 11/21/2016
 */
public class BytePerRequestReducer
        extends Reducer<Text, IntWritable, Text, ByteSizeWritable> {
    private final ByteSizeWritable byteSize = new ByteSizeWritable();

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reduce(
            final Text key,
            final Iterable<IntWritable> values,
            final Context context) throws IOException, InterruptedException {
        long totalBytes = 0;
        long counter = 0;
        for (IntWritable value : values) {
            totalBytes += value.get();
            counter += 1;
        }
        final double avgBytes = (double) totalBytes / counter;
        byteSize.setAvgSize(avgBytes);
        byteSize.setTotalSize(totalBytes);
        context.write(key, byteSize);
    }
}
