package com.epam.byteperrequest;

import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author Dmitrii_Kniazev
 * @since 11/21/2016
 */
public class BytePerRequestMapper
        extends Mapper<LongWritable, Text, Text, IntWritable> {
    private static final UserAgentStringParser USER_AGENT_PARSER =
            UADetectorServiceFactory.getResourceModuleParser();
    private final Text outputKey = new Text();
    private final IntWritable outputValue = new IntWritable();

    /**
     * {@inheritDoc}
     */
    @Override
    protected void map(
            final LongWritable key,
            final Text value,
            final Context context)
            throws IOException, InterruptedException {

        final String line = value.toString();
        final WebClientsCounters webClient = parseStringAndSetValues(line, outputKey, outputValue);
        context.getCounter(webClient).increment(1L);
        context.write(outputKey, outputValue);
    }

    /**
     * Parse input string and set key and value, return used web client<br/>
     *
     * @param inputStr input string
     * @return type of web client {@link WebClientsCounters}
     */
    private WebClientsCounters parseStringAndSetValues(
            final String inputStr,
            final Text key,
            final IntWritable value) {
        //Input example
        //ip1 - - [24/Apr/2011:04:06:01 -0400] "GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1" 200 40028 "-" "Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)"
        //ip46 - - [27/Apr/2011:22:24:05 -0400] "GET /sunFAQ/faq_framebuffer/framebuffer.html HTTP/1.1" 304 - "-" "Sogou web spider/4.0(+http://www.sogou.com/docs/help/webmasters.htm#07)"
        //ip1565 - - [27/Apr/2011:22:27:27 -0400] "GET /sun_ipx/ HTTP/1.1" 200 11586 "-" "MLBot (www.metadatalabs.com/mlbot)"
        //ip1566 - - [27/Apr/2011:22:28:50 -0400] "HEAD /sunFAQ/serial/ HTTP/1.0" 200 0 "-" "nph-churl"
        //ip19 - - [24/Apr/2011:05:14:36 -0400] "GET /vanagon/VanagonProTraining/DigiFant/016.jpg HTTP/1.1" 200 109813 "http://www.thesamba.com/vw/forum/viewtopic.php?t=387697&highlight=flat" "Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10_5_8; en-us) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4"

        final String[] splitTest = inputStr.split(" \"");
        key.set(getIp(splitTest[0]));
        value.set(getByteSize(splitTest[1]));
        return getClientType(splitTest[3]);
    }

    /**
     * Find ip in input string<br/>
     * Example: "ip1 - - [24/Apr/2011:04:06:01 -0400]"
     * Return: "ip1"
     *
     * @param ipStr sting with ip
     * @return ip
     */
    private String getIp(final String ipStr) {
        return ipStr.split(" ", 2)[0];
    }

    /**
     * Find size of transferred bytes in input string<br/>
     * Example: "GET /~strabal/grease/photo9/927-3.jpg HTTP/1.1\" 200 40028"
     * Return: "40028"
     * Example: "GET /sunFAQ/faq_framebuffer/framebuffer.html HTTP/1.1" 304 -"
     * Return: "0"
     *
     * @param requestStr string with
     * @return number of byte or 0 if not found
     */
    private int getByteSize(final String requestStr) {
        final String byteStr = requestStr.split(" ", 5)[4];
        if (byteStr.matches("\\d+")) {
            return Integer.parseInt(byteStr);
        }
        return 0;
    }

    /**
     * Try find web client if possible.
     *
     * @param clientStr sting with client
     * @return {@code IE} or {@code MOZILLA} or {@code OTHER} value
     */
    private WebClientsCounters getClientType(final String clientStr) {
        final ReadableUserAgent agent = USER_AGENT_PARSER.parse(clientStr);
        switch (agent.getFamily()) {
            case IE:
            case IE_MOBILE:
                return WebClientsCounters.IE;
            case FIREFOX:
            case MOBILE_FIREFOX:
            case FIREFOX_BONECHO:
            case FIREFOX_GRANPARADISO:
            case FIREFOX_LORENTZ:
            case FIREFOX_MINEFIELD:
            case FIREFOX_NAMOROKA:
            case FIREFOX_SHIRETOKO:
                return WebClientsCounters.MOZILLA;
            default:
                return WebClientsCounters.OTHER;
        }
    }
}
