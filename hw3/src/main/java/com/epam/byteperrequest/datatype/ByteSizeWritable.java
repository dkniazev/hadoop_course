package com.epam.byteperrequest.datatype;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

/**
 * Write stored average and total sizes at follow text format<br/>
 * "12.4,785"<br/>
 * where: first number is {@code avgSize}; second number id {@code totalSize};
 *
 * @author Dmitrii_Kniazev
 * @since 24.11.16
 */
public class ByteSizeWritable implements Writable {
    private double avgSize;
    private long totalSize;

    /**
     * Empty constructor
     */
    public ByteSizeWritable() {
    }

    /**
     * Input string like "12.4,785". Set avg size to '12.4' and total size to '785'
     *
     * @param encodedStr encoded avg and total size
     */
    public ByteSizeWritable(String encodedStr) {
        encodeStrAndSetValues(encodedStr);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(final DataOutput out) throws IOException {
        new Text(this.toString()).write(out);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void readFields(final DataInput in) throws IOException {
        final Text encodedText = new Text();
        encodedText.readFields(in);
        encodeStrAndSetValues(encodedText.toString());
    }

    /**
     * Parse string like "12.4,785" and set avg and total sizes
     *
     * @param encodedStr input string
     */
    private void encodeStrAndSetValues(String encodedStr) {
        //1st pos: avgSize; 2nd pos: totalSize
        final String[] split = encodedStr.split(",");
        setAvgSize(new Double(split[0]));
        setTotalSize(new Long(split[1]));
    }

    public double getAvgSize() {
        return avgSize;
    }

    public void setAvgSize(double avgSize) {
        this.avgSize = avgSize;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ByteSizeWritable)) return false;

        ByteSizeWritable that = (ByteSizeWritable) o;

        return Double.compare(that.avgSize, avgSize) == 0
                && totalSize == that.totalSize;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(avgSize, totalSize);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("%.1f", getAvgSize()) + "," + getTotalSize();
    }
}
