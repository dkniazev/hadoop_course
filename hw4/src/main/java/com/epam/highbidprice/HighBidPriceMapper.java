package com.epam.highbidprice;

import net.sf.uadetector.OperatingSystemFamily;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author Dmitrii_Kniazev
 * @since 11/24/2016
 */
public class HighBidPriceMapper extends Mapper<LongWritable, Text, IntWritable, Text> {
    private static final UserAgentStringParser USER_AGENT_PARSER =
            UADetectorServiceFactory.getResourceModuleParser();
    private static final int MIN_BID_PRICE = 250;

    private final IntWritable outputKey = new IntWritable();
    private final Text outputValue = new Text();

    /**
     * {@inheritDoc}
     */
    @Override
    protected void map(
            final LongWritable key,
            final Text value,
            final Context context)
            throws IOException, InterruptedException {
        final String[] split = value.toString().split("\t");
        final int bidPrice = Integer.parseInt(split[19]);
        if (bidPrice > MIN_BID_PRICE) {
            final int cityId = Integer.parseInt(split[7]);
            outputKey.set(cityId);
            final OperatingSystemFamily osType = getOsType(split[4]);
            outputValue.set(osType.getName());
            context.write(outputKey, outputValue);
        }
    }

    /**
     * Try define operation system type by user-agent
     *
     * @param userAgentStr user-agent string
     * @return SO family {@link OperatingSystemFamily}
     */
    private OperatingSystemFamily getOsType(final String userAgentStr) {
        final ReadableUserAgent agent = USER_AGENT_PARSER.parse(userAgentStr);
        return agent.getOperatingSystem().getFamily();
    }
}
