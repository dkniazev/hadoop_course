package com.epam.highbidprice.prtitioners;

import net.sf.uadetector.OperatingSystemFamily;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * @author Dmitrii_Kniazev
 * @since 25.11.16
 */
public class OperationSystemTypePartitioner extends Partitioner<IntWritable, Text> {
    @Override
    public int getPartition(
            final IntWritable key,
            final Text value,
            final int numPartitions) {
        //this is done to avoid performing mod with 0
        if (numPartitions == 0) {
            return 0;
        }
        final OperatingSystemFamily osType
                = OperatingSystemFamily.evaluate(value.toString());

        switch (osType) {
            case WINDOWS:
                return 0;
            case LINUX:
                return 1 % numPartitions;
            case MAC_OS:
                return 2 % numPartitions;
            default:
                return 3 % numPartitions;
        }
    }
}
