package com.epam.highbidprice;

import com.epam.highbidprice.prtitioners.OperationSystemTypePartitioner;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * @author Dmitrii_Kniazev
 * @since 11/18/2016
 */
public class HighBidPriceJob extends Configured implements Tool {
    public static void main(String[] args) throws Exception {
        final int exitCode = ToolRunner.run(new HighBidPriceJob(), args);
        System.exit(exitCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int run(String[] args) throws Exception {
        final Configuration conf = getConf();

        final GenericOptionsParser optionParser = new GenericOptionsParser(conf, args);
        final String[] remainingArgs = optionParser.getRemainingArgs();
        if (remainingArgs.length < 2) {
            System.err.println("Current number of parameters" + remainingArgs.length);
            System.err.println("Usage: highbidprice <in> <out> <path_to/city.en.txt>");
            return 2;
        }

        final Job job = Job.getInstance(conf, "BytePerRequestHomework");
        if (remainingArgs.length >= 3) {
            job.addCacheFile(new Path(args[2]).toUri());
        }
        job.setPartitionerClass(OperationSystemTypePartitioner.class);

        job.setJarByClass(getClass());
        //input file
        TextInputFormat.addInputPath(job, new Path(args[0]));
        job.setInputFormatClass(TextInputFormat.class);
        //output file
        TextOutputFormat.setOutputPath(job, new Path(args[1]));
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Text.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setMapperClass(HighBidPriceMapper.class);
        job.setReducerClass(HighBidPriceReducer.class);

        final boolean isCompleted = job.waitForCompletion(true);

        if (isCompleted) {
            return 0;
        }
        return 1;
    }
}
