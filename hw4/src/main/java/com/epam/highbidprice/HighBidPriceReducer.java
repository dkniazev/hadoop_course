package com.epam.highbidprice;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitrii_Kniazev
 * @since 11/24/2016
 */
public class HighBidPriceReducer extends Reducer<IntWritable, Text, Text, Text> {
    //name of file contained cityId with his name
    private static final String CITY_CODE_FILE_NAME = "city.en.txt";
    //key - cityId; value - cityName
    private final Map<Integer, String> cityMap = new HashMap<>();
    private final Text outputValue = new Text();
    private final Text outputKey = new Text();

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reduce(
            final IntWritable key,
            final Iterable<Text> values,
            final Context context)
            throws IOException, InterruptedException {
        int counter = 0;
        for (final Text value : values) {
            counter += 1;
        }
        outputKey.set(getCityNameById(key.get()));
        outputValue.set(String.valueOf(counter));
        context.write(outputKey, outputValue);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setup(final Context context) throws IOException, InterruptedException {
        final URI[] cacheFiles = context.getCacheFiles();
        if (null != cacheFiles && cacheFiles.length > 0) {
            for (final URI uri : cacheFiles) {
                if (uri.getPath().contains(CITY_CODE_FILE_NAME)) {
                    try (final BufferedReader reader = getReaderFromUrl()) {
                        String line = reader.readLine();
                        while (line != null) {
                            final String[] split = line.split("\t");
                            final Integer cityId = new Integer(split[0]);
                            cityMap.putIfAbsent(cityId, split[1]);
                            line = reader.readLine();
                        }
                    }
                }
            }
        }
    }

    /**
     * Try to create reader for cached file.
     *
     * @return buffered reader for file specified at {@link HighBidPriceReducer#CITY_CODE_FILE_NAME}
     * @throws IOException if file can not be opened
     */
    private BufferedReader getReaderFromUrl() throws IOException {
        return new BufferedReader(new FileReader(CITY_CODE_FILE_NAME));
    }

    /**
     * Try define city name.
     *
     * @param cityId id of city
     * @return city name if it define else return his id as string
     */
    private String getCityNameById(int cityId) {
        return cityMap.getOrDefault(cityId, String.valueOf(cityId));
    }
}
