package com.epam.highbidprice;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Dmitrii_Kniazev
 * @since 11/18/2016
 */
public class HighBidPriceJobTest {
    private static final List<String> EXAMPLES_DATA = Arrays.asList(
            //cityId=234; Bidding price=277
            "2e72d1bd7185fb76d69c852c57436d37	20131019025500549	1	CAD06D3WCtf	Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)	113.117.187.*	216	234	2	33235ca84c5fee9254e6512a41b3ad5e	8bbb5a81cc3d680dd0c27cf4886ddeae	null	3061584349	728	90	OtherView	Na	5	7330	277	48	null	2259	10057,13800,13496,10079,10076,10075,10093,10129,10024,10006,10110,13776,10146,10120,10115,10063",
            //cityId=222; Bidding price=1
            "93074d8125fa8945c5a971c2374e55a8	20131019161502142	1	CAH9FYCtgQf	Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727)	119.145.140.*	216	222	1	20fc675468712705dbf5d3eda94126da	9c1ecbb8a301d89a8d85436ebf393f7f	null	mm_10982364_973726_8930541	300	250	FourthView	Na	0	7323	1	201	null	2259	10057,10059,10083,10102,10024,10006,10110,10031,10063,10116",
            //cityId=237; Bidding price=598
            "bcbc973f1a93e22de83133f360759f04	20131019134022114	1	CALAIF9UcIi	Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; SE 2.X MetaSr 1.0)	59.34.170.*	216	237	3	7ed515fe566938ee6cfbb6ebb7ea4995	ea4e49e1a4b0edabd72386ee533de32f	null	ALLINONE_F_Width2	1000	90	Na	Na	50	7336	294	50	null	2259	10059,14273,10117,10075,10083,10102,10006,10148,11423,10110,10031,10126,13403,10063",
            //cityId=223; Bidding price=500
            "104128702bf24f58a8e215002ae851af	20131019031304295	1	CANGkv9Beql	Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; QQBrowser/7.4.14018.400)	14.118.66.*	216	223	3	7ed515fe566938ee6cfbb6ebb7ea4995	a9cf8a158d7aacd8453b7a9205e87181	null	Sports_F_Width1	1000	90	Na	Na	20	7336	294	160	null	2259	10057,10059,10077,10075,10083,10006,10111,10131,10126,13403,10063,10116",
            //cityId=234; Bidding price=251
            "4c663df2781a5b53dc1afe33bd9d2f75	20131019112101547	1	D1RDdd2rc6g	Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1	183.33.95.*	216	234	3	dd4270481b753dde29898e27c7c03920	24934afe181214d5cdfe9adc7e192101	null	Ent_F_Width1	1000	90	Na	Na	70	7336	294	70	null	2259	10057,13678,13800,10059,10079,10076,10077,10075,10129,10102,10006,10024,11423,10149,10031,10111,10063,10116",
            //cityId=233; Bidding price=250
            "da57aa9205e1c57e5c99a6f657cb6b0d	20131019002000352	1	D1TMbZ0Oefa	Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)	221.4.142.*	216	233	2	adf2a07e81fd1c55205de67c326424f8	null	null	2754779263	728	90	OtherView	Na	5	7330	250	43	null	2259	16593,14273,10075,10006,13866,10110,10126,11944,13403,10063,10116",
            //cityId=225; Bidding price=300
            "684aeaff2af2c4f2c436a22f10a7be57	20131019170402848	1	D1VE19AOb4Q	Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; .NET CLR 3.0.4506.2152)	183.21.78.*	216	225	3	eb6cf22883534275b3c9b6af2307e28d	3317cc43a51962811c3c8dc32dcc970d	null	Game_F_Width1	1000	90	Na	Na	50	7336	294	166	null	2259	10048,10076"
    );
    private static final LongWritable ONE_LONG = new LongWritable(1);
    private static final IntWritable ONE_INT = new IntWritable(1);
    private static final Text WIN_TEXT = new Text("Windows");

    private MapDriver<LongWritable, Text, IntWritable, Text> mapDriver;
    private ReduceDriver<IntWritable, Text, Text, Text> reduceDriver;
    private MapReduceDriver<LongWritable, Text, IntWritable, Text, Text, Text> mapReduceDriver;

    @Before
    public void setUp() {
        final HighBidPriceMapper mapper = new HighBidPriceMapper();
        mapDriver = MapDriver.newMapDriver(mapper);

        final HighBidPriceReducer reducer = new HighBidPriceReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);

        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapper() throws IOException {
        for (final String str : EXAMPLES_DATA) {
            mapDriver.withInput(ONE_LONG, new Text(str));
        }

        mapDriver.withOutput(new IntWritable(234), WIN_TEXT);
        mapDriver.withOutput(new IntWritable(237), WIN_TEXT);
        mapDriver.withOutput(new IntWritable(223), WIN_TEXT);
        mapDriver.withOutput(new IntWritable(234), WIN_TEXT);
        mapDriver.withOutput(new IntWritable(225), WIN_TEXT);

        mapDriver.runTest();
    }

    @Test
    public void testReducer() throws IOException {
        reduceDriver.withInput(new IntWritable(1), getReducerInput(5));
        reduceDriver.withInput(new IntWritable(5), getReducerInput(100000));
        reduceDriver.withInput(new IntWritable(3), getReducerInput(7));
        reduceDriver.withInput(new IntWritable(2), getReducerInput(0));
        reduceDriver.withInput(new IntWritable(4), getReducerInput(8888));

        reduceDriver.withOutput(new Text("1"), new Text("5"));
        reduceDriver.withOutput(new Text("5"), new Text("100000"));
        reduceDriver.withOutput(new Text("3"), new Text("7"));
        reduceDriver.withOutput(new Text("4"), new Text("8888"));

        reduceDriver.runTest();
    }

    @Test
    public void testMapReduce() throws IOException {
        for (final String str : EXAMPLES_DATA) {
            mapReduceDriver.withInput(ONE_LONG, new Text(str));
        }
        mapReduceDriver.withOutput(new Text("223"), new Text("1"));
        mapReduceDriver.withOutput(new Text("225"), new Text("1"));
        mapReduceDriver.withOutput(new Text("234"), new Text("2"));
        mapReduceDriver.withOutput(new Text("237"), new Text("1"));
        mapReduceDriver.runTest();
    }

    private List<Text> getReducerInput(int num) {
        final List<Text> result = new ArrayList<>(num);
        for (int i = 0; i < num; i++) {
            result.add(WIN_TEXT);
        }
        return result;
    }
}
