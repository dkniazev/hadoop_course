#!/usr/bin/env bash
# view files in hdfs
hdfs dfs -ls /test4
# first version with shared resources
hadoop jar ~/Downloads/hadoop_course/hw4/target/highbidprice-1.0-SNAPSHOT.jar /test4 /out4 /shared/city.en.txt
# version with different reducers depending OS type and number of reducers more then 1
hadoop jar ~/Downloads/hadoop_course/hw4/target/highbidprice-1.0-SNAPSHOT.jar -Dmapreduce.job.reduces=4 /test4 /out4 /shared/city.en.txt
