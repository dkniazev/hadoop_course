package com.epam.longestword;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author Dmitrii_Kniazev
 * @since 11/15/2016
 */
class LongestWordReducer
        extends Reducer<IntWritable, Text, Text, IntWritable> {
    /**
     * {@inheritDoc}
     */
    @Override
    protected void reduce(
            final IntWritable key,
            final Iterable<Text> values,
            final Context context) throws IOException, InterruptedException {
        String maxLenWord = "";
        for (final Text word : values) {
            final String nextWord = word.toString();
            if (nextWord.length() > maxLenWord.length()) {
                maxLenWord = nextWord;
            }
        }
        context.write(new Text(maxLenWord), new IntWritable(maxLenWord.length()));
    }
}
