package com.epam.longestword;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * @author Dmitrii_Kniazev
 * @since 11/15/2016
 */
public class LongestWordJob extends Configured implements Tool {
    public static void main(String[] args) throws Exception {
        final int exitCode = ToolRunner.run(new LongestWordJob(), args);
        System.exit(exitCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int run(String[] args) throws Exception {
        final Configuration conf = getConf();

        GenericOptionsParser optionParser = new GenericOptionsParser(conf, args);
        String[] remainingArgs = optionParser.getRemainingArgs();
        if (remainingArgs.length != 2) {
            System.err.println("Current number of parameters" + remainingArgs.length);
            System.err.println("Usage: longestword <in> <out> ");
            return 2;
        }

        final Job job = Job.getInstance(conf, "LongestWordHomework");
        job.setJarByClass(getClass());

        TextInputFormat.addInputPath(job, new Path(args[0]));
        job.setInputFormatClass(TextInputFormat.class);

        TextOutputFormat.setOutputPath(job, new Path(args[1]));
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Text.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setMapperClass(LongestWordMapper.class);
        job.setReducerClass(LongestWordReducer.class);

        final boolean isCompleted = job.waitForCompletion(true);
        if (isCompleted) {
            return 0;
        }
        return 1;
    }
}
