package com.epam.longestword;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.*;

/**
 * @author Dmitrii_Kniazev
 * @since 11/15/2016
 */
class LongestWordMapper
        extends Mapper<LongWritable, Text, IntWritable, Text> {

    private static final IntWritable ONE = new IntWritable(1);
    private final Text word = new Text();

    @Override
    protected void map(
            final LongWritable key,
            final Text value,
            final Context context) throws IOException, InterruptedException {
        final String line = value.toString().replaceAll("[\\]\\[(){},.;!?<>%]", "");
        final StringTokenizer tokenizer = new StringTokenizer(line);
        while (tokenizer.hasMoreTokens()) {
            word.set(tokenizer.nextToken());
            context.write(ONE, word);
        }
    }
}
