package com.epam.longestword;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author Dmitrii_Kniazev
 * @since 11/15/2016
 */
public class LongestWordMRTest {
    private static final IntWritable ONE_INT = new IntWritable(1);
    private static final LongWritable ONE_LONG = new LongWritable(1);

    private MapDriver<LongWritable, Text, IntWritable, Text> mapDriver;
    private ReduceDriver<IntWritable, Text, Text, IntWritable> reduceDriver;
    private MapReduceDriver<LongWritable, Text, IntWritable, Text, Text, IntWritable> mapReduceDriver;

    @Before
    public void setUp() {
        final LongestWordMapper mapper = new LongestWordMapper();
        mapDriver = MapDriver.newMapDriver(mapper);

        final LongestWordReducer reducer = new LongestWordReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);

        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver.withAll(Arrays.asList(
                new Pair<>(ONE_LONG, new Text("Hello [World, Bye World)!")),
                new Pair<>(ONE_LONG, new Text("Hello Hadoop, (Goodbye to] hadoop."))
        ));
        mapDriver.withAllOutput(Arrays.asList(
                new Pair<>(ONE_INT, new Text("Hello")),
                new Pair<>(ONE_INT, new Text("World")),
                new Pair<>(ONE_INT, new Text("Bye")),
                new Pair<>(ONE_INT, new Text("World")),
                new Pair<>(ONE_INT, new Text("Hello")),
                new Pair<>(ONE_INT, new Text("Hadoop")),
                new Pair<>(ONE_INT, new Text("Goodbye")),
                new Pair<>(ONE_INT, new Text("to")),
                new Pair<>(ONE_INT, new Text("hadoop"))
        ));
        mapDriver.runTest();
    }

    @Test
    public void testReducer() throws IOException {
        reduceDriver.withInput(ONE_INT, Arrays.asList(
                new Text("Hello"),
                new Text("World"),
                new Text("Bye"),
                new Text("World"),
                new Text("Hello"),
                new Text("Hadoop"),
                new Text("Goodbye"),
                new Text("to"),
                new Text("hadoop")
        ));
        reduceDriver.withOutput(new Text("Goodbye"), new IntWritable(7));
        reduceDriver.runTest();
    }

    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver.withAll(Arrays.asList(
                new Pair<>(ONE_LONG, new Text("Hello World, Bye World!")),
                new Pair<>(ONE_LONG, new Text("Hello Hadoop, Goodbye to hadoop."))
        ));
        mapReduceDriver.withOutput(new Text("Goodbye"), new IntWritable(7));
        mapReduceDriver.runTest();
    }
}
