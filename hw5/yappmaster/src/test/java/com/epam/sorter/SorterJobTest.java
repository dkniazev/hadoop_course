package com.epam.sorter;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import sorter.SorterMapper;
import sorter.SorterReducer;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

/**
 * @author Dmitrii_Kniazev
 * @since 11/28/2016
 */
public class SorterJobTest {
    private static final NullWritable NULL_WRITABLE = NullWritable.get();
    private static final IntWritable ONE_INT = new IntWritable(1);
    private static final LongWritable ONE_LONG = new LongWritable(1);

    private MapDriver<LongWritable, Text, IntWritable, IntWritable> mapDriver;
    private ReduceDriver<IntWritable, IntWritable, Text, NullWritable> reduceDriver;
    private MapReduceDriver<LongWritable, Text, IntWritable, IntWritable, Text, NullWritable> mapReduceDriver;

    @Ignore
    @Before
    public void setUp() {
        final SorterMapper mapper = new SorterMapper();
        mapDriver = MapDriver.newMapDriver(mapper);

        final SorterReducer reducer = new SorterReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);

        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Ignore
    @Test
    public void testMapper() throws IOException {
        mapDriver.withAll(Arrays.asList(
                new Pair<>(ONE_LONG, new Text("5")),
                new Pair<>(ONE_LONG, new Text("1")),
                new Pair<>(ONE_LONG, new Text("52")),
                new Pair<>(ONE_LONG, new Text("0")),
                new Pair<>(ONE_LONG, new Text("1")),
                new Pair<>(ONE_LONG, new Text("44")),
                new Pair<>(ONE_LONG, new Text("8888888")),
                new Pair<>(ONE_LONG, new Text("1"))
        ));
        mapDriver.withAllOutput(Arrays.asList(
                new Pair<>(new IntWritable(5), ONE_INT),
                new Pair<>(new IntWritable(1), ONE_INT),
                new Pair<>(new IntWritable(52), ONE_INT),
                new Pair<>(new IntWritable(0), ONE_INT),
                new Pair<>(new IntWritable(1), ONE_INT),
                new Pair<>(new IntWritable(44), ONE_INT),
                new Pair<>(new IntWritable(8888888), ONE_INT),
                new Pair<>(new IntWritable(1), ONE_INT)
        ));
        mapDriver.runTest();
    }

    @Ignore
    @Test
    public void testReducer() throws IOException {
        reduceDriver.withInput(new IntWritable(4), Arrays.asList(ONE_INT,ONE_INT));
        reduceDriver.withInput(new IntWritable(104), Collections.singletonList(ONE_INT));

        reduceDriver.withOutput(new Text("4"), NULL_WRITABLE);
        reduceDriver.withOutput(new Text("4"), NULL_WRITABLE);
        reduceDriver.withOutput(new Text("104"), NULL_WRITABLE);
        reduceDriver.runTest();
    }

    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver.withAll(Arrays.asList(
                new Pair<>(ONE_LONG, new Text("5")),
                new Pair<>(ONE_LONG, new Text("1")),
                new Pair<>(ONE_LONG, new Text("52")),
                new Pair<>(ONE_LONG, new Text("0")),
                new Pair<>(ONE_LONG, new Text("1")),
                new Pair<>(ONE_LONG, new Text("44")),
                new Pair<>(ONE_LONG, new Text("8888888")),
                new Pair<>(ONE_LONG, new Text("1"))
        ));
        mapReduceDriver.withOutput(new Text("0"), NULL_WRITABLE);
        mapReduceDriver.withOutput(new Text("1"), NULL_WRITABLE);
        mapReduceDriver.withOutput(new Text("1"), NULL_WRITABLE);
        mapReduceDriver.withOutput(new Text("1"), NULL_WRITABLE);
        mapReduceDriver.withOutput(new Text("5"), NULL_WRITABLE);
        mapReduceDriver.withOutput(new Text("44"), NULL_WRITABLE);
        mapReduceDriver.withOutput(new Text("52"), NULL_WRITABLE);
        mapReduceDriver.withOutput(new Text("8888888"), NULL_WRITABLE);
        mapReduceDriver.runTest();
    }
}
