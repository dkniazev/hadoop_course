package generator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * @author Dmitrii_Kniazev
 * @since 29.11.16
 */
public class Generator {
    private static final Logger LOGGER = Logger.getLogger(Generator.class);

    private static final int MAX_LEN = 1_000_000_000;
    private static final Random RANDOMIZER = new Random();

    public static void main(String[] args) throws IOException, URISyntaxException {
        final long startTime = System.currentTimeMillis();

        final String outUri = "/hw5/testfile.txt";

        final Configuration conf = new Configuration();
        conf.set("fs.defaultFS", "hdfs://localhost:54310");
        conf.set("fs.hdfs.impl",
                org.apache.hadoop.hdfs.DistributedFileSystem.class.getName()
        );

        try (final FileSystem fs = DistributedFileSystem.get(conf)) {
            try (final BufferedWriter writer = getWriter(fs, new Path(outUri))) {
                RANDOMIZER.ints(MAX_LEN).forEach(value -> {
                    try {
                        writer.write(value + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
            LOGGER.debug("Output file write");
            LOGGER.debug("All done");
        }
        final long endTime = System.currentTimeMillis();
        System.out.println("Job Time Duration:" + (endTime - startTime));
    }

    /**
     * Create file to write. Overwrite it if exist.
     *
     * @param fs   file system
     * @param path to file
     * @return buffered writer for specified file path
     * @throws IOException if file can not be create
     */
    private static BufferedWriter getWriter(final FileSystem fs, final Path path)
            throws IOException {
        return new BufferedWriter(new OutputStreamWriter(fs.create(path, true)));
    }
}
