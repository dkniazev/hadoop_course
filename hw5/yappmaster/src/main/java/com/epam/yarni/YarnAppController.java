package com.epam.yarni;

import com.google.inject.Inject;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.yarn.api.ApplicationConstants;
import org.apache.hadoop.yarn.api.protocolrecords.AllocateResponse;
import org.apache.hadoop.yarn.api.records.*;
import org.apache.hadoop.yarn.client.api.AMRMClient;
import org.apache.hadoop.yarn.client.api.NMClient;
import org.apache.hadoop.yarn.client.api.YarnClient;
import org.apache.hadoop.yarn.exceptions.YarnException;
import org.apache.hadoop.yarn.util.ConverterUtils;
import org.apache.hadoop.yarn.util.Records;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;


import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitrii_Kniazev
 * @since 29.11.16
 */
@Controller
public class YarnAppController {

    private static final String APP_FILE = "simpleapp.jar";
    private static final Logger LOG = Logger.getLogger(YarnAppController.class);

    final private AMRMClient<AMRMClient.ContainerRequest> amClient;
    final private NMClient nmClient;
    final private Resource maxResources;
    final private ApplicationContext applicationContext;

    @Autowired
    public YarnAppController(
            AMRMClient<AMRMClient.ContainerRequest> amClient,
            NMClient nmClient,
            Resource maxResources,
            ApplicationContext applicationContext) {
        this.amClient = amClient;
        this.nmClient = nmClient;
        this.maxResources = maxResources;
        this.applicationContext = applicationContext;
    }

    @RequestMapping("/info")
    public String info(Model model) throws IOException, YarnException {
        LOG.info("Open info page");
        return "info";
    }

    @RequestMapping("/start")
    public String start(
            @RequestParam(value = "cores") String cores,
            @RequestParam(value = "ram") String ram,
            @RequestParam(value = "priority") String priority,
            Model model)
            throws Exception {
        LOG.info("Start computation");
        LOG.info("Cores:" + cores);
        LOG.info("Ram:" + ram);
        LOG.info("Priority:" + priority);

        final int coresInt = Integer.parseInt(cores);
        final int ramInt = Integer.parseInt(ram);
        final int priorityInt = Integer.parseInt(priority);

        // Resource requirements for worker containers
        final Resource resources = Records.newRecord(Resource.class);
        resources.setMemory(ramInt);
        resources.setVirtualCores(coresInt);

        // Priority for worker containers - priorities are intra-application
        final Priority priorityVal = Records.newRecord(Priority.class);
        priorityVal.setPriority(priorityInt);

        final AMRMClient.ContainerRequest containerAsk = new AMRMClient.ContainerRequest(
                resources,
                null,
                null,
                priorityVal);
        amClient.addContainerRequest(containerAsk);
        LOG.info("Add container request");


        model.addAttribute("cores", cores);
        model.addAttribute("ram", ram);
        model.addAttribute("priority", priority);
        model.addAttribute("result", "failed");

        // Check resources
        if (ramInt < 0 || ramInt > maxResources.getMemory()) {
            LOG.error("requested " + ramInt + " mb, available " + maxResources.getMemory() + " mb");
            model.addAttribute("result", "failed by memory");
            return "start";
        }

        if (coresInt < 0 || coresInt > maxResources.getVirtualCores()) {
            LOG.error("requested " + coresInt + " cores, available " + maxResources.getVirtualCores() + " cores");
            model.addAttribute("result", "failed by cores");
            return "start";
        }

        final AllocateResponse response = amClient.allocate(0.5f);
        if (response.getAllocatedContainers().size() > 0) {
            amClient.removeContainerRequest(containerAsk);
            final Container container = response.getAllocatedContainers().get(0);
            LOG.info("Allocate container");
            final ContainerLaunchContext context = Records.newRecord(ContainerLaunchContext.class);
            final Configuration conf = amClient.getConfig();
            final Map<String, LocalResource> localResources = new HashMap<>();
            addToLocalResources(conf, APP_FILE, APP_FILE, localResources);
            context.setLocalResources(localResources);

            context.setCommands(Collections.singletonList(getCommand(ramInt)));
            nmClient.startContainer(container, context);
            model.addAttribute("result", "started");
        }
        return "start";
    }

    @RequestMapping("/close")
    public String close(Model model) {
        try {
            amClient.unregisterApplicationMaster(FinalApplicationStatus.SUCCEEDED, "SUCCEEDED", "");
            LOG.info("Application manager SUCCEEDED closed");
            SpringApplication.exit(applicationContext);

        } catch (YarnException | IOException e) {
            LOG.error("Application manager ERROR while closed");
            LOG.error(e.getMessage(), e);
        }
        return "close";
    }

    private static void addToLocalResources(
            final Configuration conf,
            final String fileSrcPath,
            final String fileDstPath,
            final Map<String, LocalResource> localResources)
            throws Exception {
        final FileSystem fs = FileSystem.get(conf);
        final Path dst = new Path(fs.getHomeDirectory(), fileDstPath);
        fs.copyFromLocalFile(false, true, new Path(fileSrcPath), dst);
        final FileStatus scFileStatus = fs.getFileStatus(dst);
        final LocalResource scRsrc = LocalResource.newInstance(
                ConverterUtils.getYarnUrlFromURI(dst.toUri()),
                LocalResourceType.FILE, LocalResourceVisibility.PUBLIC,
                scFileStatus.getLen(), scFileStatus.getModificationTime()
        );
        localResources.put(fileDstPath, scRsrc);
    }

    private String getCommand(int ram) {
        return "$JAVA_HOME/bin/java" +
                " -Xmx" + ram + "M" +
                " -cp " + APP_FILE +
                " -Dloader.main=sorter.SorterJob" +
                " org.springframework.boot.loader.PropertiesLauncher" +
                " /hw5/testfile.txt" +
                " /out5" +
                " 1>" + ApplicationConstants.LOG_DIR_EXPANSION_VAR + "/stdout" +
                " 2>" + ApplicationConstants.LOG_DIR_EXPANSION_VAR + "/stderr";
    }
}
