package com.epam.yarni;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.yarn.api.protocolrecords.RegisterApplicationMasterResponse;
import org.apache.hadoop.yarn.api.records.Resource;
import org.apache.hadoop.yarn.client.api.AMRMClient;
import org.apache.hadoop.yarn.client.api.NMClient;
import org.apache.hadoop.yarn.conf.YarnConfiguration;
import org.apache.hadoop.yarn.exceptions.YarnException;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.net.InetAddress;

/**
 * @author Dmitry
 * @since 27.11.2016
 */
@SpringBootApplication
public class YAppMaster {
    private static final Logger LOG = Logger.getLogger(YAppMaster.class);

    private final Configuration conf;

    private final AMRMClient<AMRMClient.ContainerRequest> amClient;

    private final NMClient nmClient;

    private final Resource maxResources;

    public YAppMaster() throws IOException, YarnException {
        this.conf = new YarnConfiguration();
        LOG.info("Start YAppMaster");
        final String hadoopHome = System.getenv("HADOOP_HOME");
        if (hadoopHome != null) {
            conf.addResource(new Path(hadoopHome + "/etc/hadoop/yarn-site.xml"));
            conf.addResource(new Path(hadoopHome + "/etc/hadoop/core-site.xml"));
        }

        amClient = AMRMClient.createAMRMClient();
        amClient.init(conf);
        amClient.start();

        nmClient = NMClient.createNMClient();
        nmClient.init(conf);
        nmClient.start();

        // Register with ResourceManager
        String hostName = InetAddress.getLocalHost().getHostName();
        LOG.info("Register application master at " + hostName + ":8080");
        RegisterApplicationMasterResponse response = amClient
                .registerApplicationMaster(
                        hostName,
                        8080,
                        "http://" + hostName + ":8080/");
        maxResources = response.getMaximumResourceCapability();
    }

    @Bean
    public AMRMClient<AMRMClient.ContainerRequest> getAmClient() {
        return amClient;
    }

    @Bean
    public NMClient getNmClient() {
        return nmClient;
    }

    @Bean
    public Resource getMaxResources() {
        return maxResources;
    }

    public static void main(String[] args) {
        SpringApplication.run(YAppMaster.class, args);
    }
}
