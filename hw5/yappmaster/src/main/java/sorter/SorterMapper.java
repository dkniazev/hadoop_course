package sorter;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author Dmitrii_Kniazev
 * @since 11/28/2016
 */
public class SorterMapper
        extends Mapper<LongWritable, Text, IntWritable, IntWritable> {
    private static final IntWritable ONE = new IntWritable(1);
    private final IntWritable keyOutput = new IntWritable();

    @Override
    protected void map(
            final LongWritable key,
            final Text value,
            final Context context)
            throws IOException, InterruptedException {
        keyOutput.set(Integer.parseInt(value.toString()));
        context.write(keyOutput, ONE);
    }
}
