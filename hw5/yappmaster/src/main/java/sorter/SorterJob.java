package sorter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * @author Dmitrii_Kniazev
 * @since 11/28/2016
 */
public class SorterJob extends Configured implements Tool {
    public static void main(String[] args) throws Exception {
        final int exitCode = ToolRunner.run(new SorterJob(), args);
        System.exit(exitCode);
    }

    @Override
    public int run(String[] args) throws Exception {
        final Configuration conf = getConf();
        conf.set("fs.defaultFS", "hdfs://localhost:54310");
        conf.set("fs.hdfs.impl",
                org.apache.hadoop.hdfs.DistributedFileSystem.class.getName()
        );
        final String hadoopHome = System.getenv("HADOOP_HOME");
        if (hadoopHome != null) {
            conf.addResource(new Path(hadoopHome + "/etc/hadoop/core-site.xml"));
        }

        GenericOptionsParser optionParser = new GenericOptionsParser(conf, args);
        String[] remainingArgs = optionParser.getRemainingArgs();
        if (remainingArgs.length != 2) {
            System.err.println("Current number of parameters" + remainingArgs.length);
            System.err.println("Usage: sorterjob <in> <out> ");
            return 2;
        }

        final Job job = Job.getInstance(conf, "SorterJob");
        job.setJarByClass(getClass());
        job.setNumReduceTasks(1);

        //SequenceFileAsBinaryInputFormat
        TextInputFormat.addInputPath(job, new Path(args[0]));
        job.setInputFormatClass(TextInputFormat.class);

        TextOutputFormat.setOutputPath(job, new Path(args[1]));
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(NullWritable.class);

        job.setMapperClass(SorterMapper.class);
        job.setReducerClass(SorterReducer.class);

        final boolean isCompleted = job.waitForCompletion(true);
        if (isCompleted) {
            return 0;
        }
        return 1;
    }
}
