package sorter;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author Dmitrii_Kniazev
 * @since 11/28/2016
 */
public class SorterReducer
        extends Reducer<IntWritable, IntWritable, Text, NullWritable> {

    private static final NullWritable NULL_WRITABLE = NullWritable.get();
    private static final int MAX_RESULT_LEN = 100;
    private final Text keyOutput = new Text();

    @Override
    protected void reduce(
            final IntWritable key,
            final Iterable<IntWritable> values,
            final Context context)
            throws IOException, InterruptedException {
        final String keyAsString = key.toString();

        for (final IntWritable value : values) {
            if (context.getCounter(ResCounters.ANSWERS).getValue() >= MAX_RESULT_LEN) {
                break;
            }
            keyOutput.set(keyAsString);
            context.write(keyOutput, NULL_WRITABLE);
            context.getCounter(ResCounters.ANSWERS).increment(1L);
        }
    }
}
