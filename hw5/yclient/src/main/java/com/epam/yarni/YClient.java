package com.epam.yarni;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.yarn.api.ApplicationConstants;
import org.apache.hadoop.yarn.api.records.*;
import org.apache.hadoop.yarn.client.api.YarnClient;
import org.apache.hadoop.yarn.client.api.YarnClientApplication;
import org.apache.hadoop.yarn.conf.YarnConfiguration;
import org.apache.hadoop.yarn.util.ConverterUtils;
import org.apache.hadoop.yarn.util.Records;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry
 * @since 27.11.2016
 */
public class YClient {
    private static final Logger LOG = Logger.getLogger(YClient.class);
    private static final int MEMORY_AM = 256;
    private static final int V_CORES_AM = 1;
    private static final String APP_FILE = "simpleapp.jar";

    private final Configuration conf;

    private YClient() {
        conf = new YarnConfiguration();
    }

    private void run(String[] args) throws Exception {
        final String jarPath = args[0];

        // Create yarnClient
        final YarnClient yarnClient = YarnClient.createYarnClient();
        yarnClient.init(conf);
        yarnClient.start();

        // Create application via yarnClient
        final YarnClientApplication app = yarnClient.createApplication();

        // Set up the container launch context for the application master
        final ContainerLaunchContext amContainer =
                Records.newRecord(ContainerLaunchContext.class);
        amContainer.setCommands(
                Collections.singletonList(getCommand())
        );

        // Setup jar for ApplicationMaster
        final Map<String, LocalResource> localResources = new HashMap<>();
        addToLocalResources(conf, jarPath, APP_FILE, localResources);
        amContainer.setLocalResources(localResources);

        // Set up resource type requirements for ApplicationMaster
        final Resource capability = Records.newRecord(Resource.class);
        capability.setMemory(MEMORY_AM);
        capability.setVirtualCores(V_CORES_AM);

        // Finally, set-up ApplicationSubmissionContext for the application
        final ApplicationSubmissionContext appContext =
                app.getApplicationSubmissionContext();
        appContext.setApplicationName("SimpleYarnApplication"); // application name
        appContext.setAMContainerSpec(amContainer);
        appContext.setResource(capability);
        appContext.setQueue("default"); // queue

        // Submit application
        final ApplicationId appId = appContext.getApplicationId();
        LOG.info("Submitting application " + appId);
        yarnClient.submitApplication(appContext);

        ApplicationReport appReport = yarnClient.getApplicationReport(appId);
        YarnApplicationState appState = appReport.getYarnApplicationState();
        while (appState != YarnApplicationState.FINISHED &&
                appState != YarnApplicationState.KILLED &&
                appState != YarnApplicationState.FAILED) {
            Thread.sleep(100);
            appReport = yarnClient.getApplicationReport(appId);
            appState = appReport.getYarnApplicationState();
        }

        LOG.info(
                "Application " + appId + " finished with" +
                        " state " + appState +
                        " at " + appReport.getFinishTime());

    }

    private String getCommand() {
        return "$JAVA_HOME/bin/java" +
                " -Xmx256M" +
                " -cp " + APP_FILE +
                " -Dloader.main=com.epam.yarni.YAppMaster" +
                " org.springframework.boot.loader.PropertiesLauncher" +
                " 1>" + ApplicationConstants.LOG_DIR_EXPANSION_VAR + "/stdout" +
                " 2>" + ApplicationConstants.LOG_DIR_EXPANSION_VAR + "/stderr";
    }

    private static void addToLocalResources(
            final Configuration conf,
            final String fileSrcPath,
            final String fileDstPath,
            final Map<String, LocalResource> localResources)
            throws Exception {
        final FileSystem fs = FileSystem.get(conf);
        final Path dst = new Path(fs.getHomeDirectory(), fileDstPath);
        fs.copyFromLocalFile(false, true, new Path(fileSrcPath), dst);
        final FileStatus scFileStatus = fs.getFileStatus(dst);
        final LocalResource scRsrc = LocalResource.newInstance(
                ConverterUtils.getYarnUrlFromURI(dst.toUri()),
                LocalResourceType.FILE, LocalResourceVisibility.PUBLIC,
                scFileStatus.getLen(), scFileStatus.getModificationTime()
        );
        localResources.put(fileDstPath, scRsrc);
    }

    public static void main(String[] args) throws Exception {
        final YClient client = new YClient();
        client.run(args);
    }
}
