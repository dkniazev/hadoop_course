package com.epam.recordcounter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Dmitrii_Kniazev
 * @since 11/15/2016
 */
public class RecordCounter {
    private static final Logger LOGGER = Logger.getLogger(RecordCounter.class);

    public static void main(String[] args) throws IOException, URISyntaxException {
        long startTime = System.currentTimeMillis();
        LOGGER.debug("Program started");
        if (args.length < 2) {
            LOGGER.error("Current number of parameters" + args.length);
            LOGGER.error("Usage: longestword <in> <out> [fsDefaultName]");
            for (String arg : args) {
                System.out.println(arg);
            }
            System.exit(2);
        }
        final String inUri = args[0];
        final String outUri = args[1] + "/bid_result.txt";

        final Configuration conf = new Configuration();
        if (args.length == 3) {
            conf.set("fs.defaultFS", args[2]);
        } else {
            conf.set("fs.defaultFS", "hdfs://sandbox.hortonworks.com:8020");
        }

        conf.set("fs.hdfs.impl",
                org.apache.hadoop.hdfs.DistributedFileSystem.class.getName()
        );

        final Map<String, Integer> idsMap = new HashMap<>();
        try (final FileSystem fs = DistributedFileSystem.get(conf)) {
            //find all files in directory
            final FileStatus[] fileStatuses = fs.listStatus(new Path(inUri));
            for (final FileStatus status : fileStatuses) {

                LOGGER.debug(status.getPath().toString());
                //read file
                try (final BufferedReader reader = getReader(fs, status.getPath())) {
                    String line = reader.readLine();
                    while (line != null) {
                        final String[] strings = line.split("\t", 4);
                        final String iPinyou_ID = strings[2];
                        if (!iPinyou_ID.equals("null")) {
                            final Integer counter = idsMap.getOrDefault(iPinyou_ID, 0);
                            idsMap.put(iPinyou_ID, counter + 1);
                        }
                        line = reader.readLine();
                    }
                }
            }
            LOGGER.debug("All data read");
            final List<Map.Entry<String, Integer>> resultList = idsMap.entrySet()
                    .stream()
                    .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                    .limit(100)
                    .collect(Collectors.toList());
            LOGGER.debug("All data sorted");
            try (final BufferedWriter writer = getWriter(fs, new Path(outUri))) {
                for (Map.Entry<String, Integer> entry : resultList) {
                    writer.write(entry.getKey() + " " + entry.getValue() + "\n");
                }
            }
            LOGGER.debug("Output file write");
            LOGGER.debug("All done");
        }
        final long endTime = System.currentTimeMillis();
        System.out.println("Job Time Duration:" + (endTime - startTime));
    }

    /**
     * Open file to read.
     *
     * @param fs   file system
     * @param path to file
     * @return buffered reader for specified file path
     * @throws IOException if file can not be read
     */
    private static BufferedReader getReader(final FileSystem fs, final Path path)
            throws IOException {
        return new BufferedReader(new InputStreamReader(fs.open(path)));
    }

    /**
     * Create file to write. Overwrite it if exist.
     *
     * @param fs   file system
     * @param path to file
     * @return buffered writer for specified file path
     * @throws IOException if file can not be create
     */
    private static BufferedWriter getWriter(final FileSystem fs, final Path path)
            throws IOException {
        return new BufferedWriter(new OutputStreamWriter(fs.create(path, true)));
    }
}
